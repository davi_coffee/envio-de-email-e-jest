const express = require("express");
const bodyParser = require("body-parser");
const upload = require("multer")();
const sendEmailMailer = require("./nodemail");

const app = express();

app.use(bodyParser.json());

app.get("/", (req, res) => {
  const message =
    "<div><h1>Bem-vindo a API para envio de email</h1><p>Para mais informações: https://gitlab.com/davi_coffee/envio-de-email-e-jest</p></div>";
  return res.status(200).send(message);
});

app.post("/send", upload.array("attachments", 10), (req, res) => {
  const { name, email, message, htmlMessage } = req.body;
  const files = req.files;

  sendEmailMailer(name, email, message, htmlMessage, files)
    .then((data) => {
      return res.status(200).json(data);
    })
    .catch((err) => {
      return res.status(400).json(err);
    });
});

const server = app.listen(3001);

module.exports = server;
