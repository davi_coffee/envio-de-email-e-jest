# API para envio de email com Nodemailer

**Este é um back-end para uma pequena aplicação pessoal. O objetivo desse aplicação é criar uma API com apenas um endpoint para envio de email com o serviço gratuito de webmail - Gmail.**

***
***

## Tecnologias

Essa aplicação foi criada com auxílio de variáveis de ambientes, por questão de segurança, e com as seguintes tecnologias:

- Node.js
- Express.js
- Body-parser
- Nodemailer (Lib p/ envio de email com serviço SMTP)
- Multer (Lib p/ upload de arquivos)
- Jest
- Supertest


***
***

## **EndPoints**

Foram criadas duas endpoints para essa API, sendo que nenhumas das rotas é necessário autentificação.

***

### Home

`GET / - FORMATO DA RESPOSTA - STATUS 200`

Caso tudo de certa na requisição a resposta será:

![Texto alternativo](./imagens/get.png)


***

### Send

`POST /send - FORMATO DA REQUISIÇÃO`

```
{
	"name": "Fulano de Silva",
	"email":"fulano@gmail.com",
	"message": "Mensagem alternativo do email",
  "htmlMessage": "<h1>Mensagem do email</h1>",
  "attachments": [files]
}
```
![Texto alternativo](./imagens/post_multipart.png)

Caso tudo de certa na requisição a resposta será:

`POST /send - FORMATO DA RESPOSTA - STATUS 200`

```
{
  "accepted": [
    "fulano@gmail.com"
  ],
  "rejected": [],
  "envelopeTime": 779,
  "messageTime": 13004,
  "messageSize": 2920426,
  "response": "250 2.0.0 OK  1615078263 z1sm4870737qtq.78 - gsmtp",
  "envelope": {
    "from": "code.easy.31415@gmail.com",
    "to": [
      "fulano@gmail.com"
    ]
  },
  "messageId": "<8354ca35-4ad1-14a2-ce0d-dae95f143208@gmail.com>"
}
```
![Texto alternativo](./imagens/post_respost.png)

**Especificações do campos:**
> Para enviar um email é obrigatório no minímo o email, sendo assim os outros campos não são requiridos e cabe a utilização desse campos conforme o problema.

1. name
    - String
    - Título do que será enviado

2. email
    - String
    - Email que irá receber a mensagem

3. message
    - String
    - Caso não colocado htmlMessage ou tenha algum erro, o texto da messagem será esse campo.

4. htmlMessage
    - String
    - Corpo da mensagem que será enviado no formato de HTML

5. attachments
    - Objetos de anexos
    - Tem um limite de 10 anexos, podendo ser qualquer arquivo suportado pelo gmail.

## Conclusão

Api é muito simples a sua utilização, foi apenas construída para enviar email com uso de um formulário para ser colocado em sites e dessa forma ter uma comunicação fácil e rápida cliente - vendedor.

### TaskList

- [x] Construir Servidor e suas dependências
- [x] Adicionar a função sendEmail com Nodemailer
- [x] Inserir rota send
- [x] Inserir callback upload para envio de arquivos
- [x] Criar testes TDD
