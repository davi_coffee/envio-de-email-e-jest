require("dotenv/config");
const mailer = require("nodemailer");

const sendEmailMailer = (name, email, message, htmlMessage, files) => {
  const smtpTramnsport = mailer.createTransport({
    host: "smtp.gmail.com",
    port: "587",
    secure: false,
    auth: {
      user: process.env.MY_EMAIL,
      pass: process.env.MY_PASS,
    },
  });

  smtpTramnsport.verify((err, _sucess) => {
    if (err) {
      return new Promise((_resolve, reject) => reject(new Error(err)));
    }
  });

  const mail = {
    from: process.env.EMAIL_FROM,
    to: email,
    subject: `${name} te enviou uma mensagem`,
    text: message,
    html: htmlMessage,
  };

  if (files) {
    mail.attachments = [];
    for (let i = 0; i < files.length; i++) {
      mail.attachments.push({
        filename: files[i].originalname,
        content: files[i].buffer,
      });
    }
  }

  return new Promise((resolve, reject) => {
    smtpTramnsport
      .sendMail(mail)
      .then((data) => {
        smtpTramnsport.close();
        return resolve(data);
      })
      .catch((err) => {
        smtpTramnsport.close();
        return reject(err);
      });
  });
};

module.exports = sendEmailMailer;
