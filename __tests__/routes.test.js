require("dotenv/config");
const sendEmailMailer = require("../nodemail");
const server = require("../server");
const request = require("supertest");

const paramsPost = {
  email: process.env.EMAIL_TEST,
  name: "Teste",
  message: undefined,
  htmlMessage: "<h1>Teste da rota Send</h1>",
  attachments: undefined,
};
const expected = {
  accepted: [paramsPost.email],
  envelope: {
    from: process.env.EMAIL_TEST_FROM,
    to: [paramsPost.email],
  },
};

// Setup
beforeAll(async () => {
  console.log("Iniciando pilha de test TDD com JEST");
});

// Turn Down
afterAll(() => {
  server.close();
  console.log("Finalizado os testes");
});

describe("Início dos testes", () => {
  test("Acesso a rota home e verificação do conteúdo exibido", async () => {
    const { status, text } = await request(server).get("/");
    const message =
      "<div><h1>Bem-vindo a API para envio de email</h1><p>Para mais informações: URL</p></div>";
    expect(status).toEqual(200);
    expect(text).toContain(message);
  });

  test("Acessa a rota send e verifica seu retorno", async () => {
    const { status, body } = await request(server)
      .post("/send")
      .send(paramsPost);

    expect(status).toEqual(200);
    expect(body).toEqual(expect.objectContaining(expected));
  });

  test("Verifica o retorno da função sendEmailMailer", async () => {
    expect(
      sendEmailMailer(
        paramsPost.name,
        paramsPost.email,
        paramsPost.message,
        paramsPost.htmlMessage,
        paramsPost.attachments
      )
    ).resolves.toEqual(expect.objectContaining(expected));
  });
});
